import React from "react";
import Title from "../Title";
import aboutBcg from "../../images/aboutBcg.jpeg";

export default function info() {
  return (
    <section className="py-5">
      <div className="container">
        <div className="row">
          <div className="col-10 mx-auto col-md-6 my-3">
            <img
              src={aboutBcg}
              className="img-fluid img-thumbnail"
              alt="Get to know me"
              style={{ background: "var(--darkGrey" }}
            />
          </div>
          <div className="col-10 mx-auto col-md-6 my-3">
            <Title title="about us"></Title>
            <p className="text-lead text-muted my-3">
              Lorem ipsum dolor sit amet consectetur, adipisicing elit. Id porro
              vel illum sapiente voluptatum, quibusdam cumque nobis, repellat,
              laudantium dignissimos fugiat. A, voluptatum nesciunt. Iusto id
              architecto atque magni quasi harum non error aut tempora odio!
              Ipsum at optio laudantium recusandae voluptatibus quam eveniet
              iusto, totam, exercitationem quidem, sequi expedita nostrum odit!
              Facilis incidunt aperiam illum praesentium quod est iure?
            </p>

            <button
              className="main-link"
              type="button"
              style={{ marginTop: "2rem" }}
            >
              more info
            </button>
          </div>
        </div>
      </div>
    </section>
  );
}
